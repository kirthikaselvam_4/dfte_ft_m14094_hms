package com.training.hospitalmanagementsystem.controller;

import java.io.IOException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.hospitalmanagementsystem.config.AppointmentNotFoundException;
import com.training.hospitalmanagementsystem.model.Appointment;
import com.training.hospitalmanagementsystem.repository.AppointmentRepository;
//import com.training.hospitalmanagementsystem.repository.AppointmentRepository;
import com.training.hospitalmanagementsystem.service.AppointmentService;

@RestController
@RequestMapping("/Appointment")
public class AppointmentController {
	@Autowired
	AppointmentRepository appointmentRepository;
	
	//AppointmentService
	@Autowired 
	AppointmentService appointmentService;

	@GetMapping("/all")
	//@ExceptionHandler({Exception.class})
	public List<Appointment> getAllAppointmentDetails() {
		return appointmentService.getAllAppointmentDetails();
	}
	
	
	
	@GetMapping(value = "/{AppointmentId}")

	public Appointment getOneAppointment(@PathVariable Long AppointmentId) {
		if(!appointmentRepository.existsById(AppointmentId))throw new AppointmentNotFoundException();
		return appointmentService.getOneAppointment(AppointmentId);
	}

	@PostMapping(value = "/")
	//@ExceptionHandler({Exception.class})
	public ResponseEntity<Appointment> createAppointmentDetails(@Valid @RequestBody Appointment appointment)  {
	    
		return new ResponseEntity<Appointment>(appointmentService.createAppointmentDetails(appointment), HttpStatus.CREATED);
		
	}
	

	@PutMapping(value = "/")
	public ResponseEntity<Appointment>  updateAppointmentDetails(@Valid @RequestBody Appointment appointment) {
		if(!appointmentRepository.existsById((appointment.getAppointmentId())))
			throw new AppointmentNotFoundException();
		return new ResponseEntity<Appointment>(appointmentService.updateAppointmentDetails(appointment),HttpStatus.CREATED);
	}

	/*@DeleteMapping(value = "/{AppointmentId}")
	public String deleteAppointmentDetailsById(@PathVariable Long AppointmentId) {
		if (appointmentService.DeleteAppointmentdetails(AppointmentId)) {
			
			return "Deleted Successfully";
		}
		return "cannot delete details";
	}*/
	@DeleteMapping(value = "/{appointmentId}")
	public String deleteAppointmentDetailsById(@PathVariable Long appointmentId) {
	try {
		if (appointmentService.deleteAppointment(appointmentId)) {
		return "Deleted Successfully";
		}
		} catch (Exception e) {
		}
		return "Not able to delete the Appointment";
		}
}
