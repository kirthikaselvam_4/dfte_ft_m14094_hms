package com.training.hospitalmanagementsystem.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.hospitalmanagementsystem.service.HospitalService;
import com.training.hospitalmanagementsystem.config.HospitalNotFoundException;
import com.training.hospitalmanagementsystem.config.PatientNotFoundException;
import com.training.hospitalmanagementsystem.model.Hospital;
import com.training.hospitalmanagementsystem.repository.HospitalRepository;

//import com.training.springbootrest.service.CustomerService;
@RestController
@RequestMapping("/Hospital")
public class HospitalController {
	@Autowired
	private HospitalRepository hospitalRepository;

	@Autowired
	HospitalService hospitalService;



	@GetMapping(value = "/{hospitalId}")

	public Hospital getHospitalDetailsById(@PathVariable Long hospitalId) {
		
		if(!hospitalRepository.existsById(hospitalId))throw new HospitalNotFoundException();
				return hospitalService.getHospitalById(hospitalId);
	}

	@GetMapping("/all")
	public List<Hospital> getAllHospitalDetails() {
		return hospitalService.getAllHospitalDetails();

	}
	

	@PostMapping(value = "/")
	public ResponseEntity<Hospital> createHospital(@Valid @RequestBody Hospital hospital) {
		return new ResponseEntity<Hospital>(hospitalService.createHospital(hospital),HttpStatus.CREATED);
	}

	@PutMapping(value = "/")
	public ResponseEntity<Hospital> updateHospital(@Valid @RequestBody Hospital hospital) {
		if(!hospitalRepository.existsById((hospital.getHospitalId())))
			throw new HospitalNotFoundException();
		return new ResponseEntity<Hospital> (hospitalService.updateHospital(hospital),HttpStatus.CREATED);
	}

	@DeleteMapping(value = "/{hospitalId}")
	public String deleteDetailsById(@PathVariable Long hospitalId) {
		try {
		if (hospitalService.DeleteHospitalById(hospitalId)) {
			
			return "Deleted Successfully";
		}
		}catch (Exception e) {	}
		return "cannot delete details";
	}
	}

