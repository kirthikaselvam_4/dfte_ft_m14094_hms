package com.training.hospitalmanagementsystem.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.hospitalmanagementsystem.config.PatientNotFoundException;
import com.training.hospitalmanagementsystem.model.Patient;
import com.training.hospitalmanagementsystem.repository.PatientRepository;
import com.training.hospitalmanagementsystem.service.PatientService;

@RestController
@RequestMapping("/Patient")
public class PatientController {
	@Autowired
	PatientRepository patientRepository;
	@Autowired
	PatientService patientService;

	@GetMapping("/all")
	public List<Patient> getAllPatientDetails() {
		return patientService.getAllPatientDetails();

	}
	
	@GetMapping(value = "/{patientId}")
	public Patient getOne(@PathVariable Long patientId) {
		if(!patientRepository.existsById(patientId))throw new PatientNotFoundException();
		return patientService.getOnePatient(patientId);
	}

	
	
	@PostMapping(value = "/")
	public ResponseEntity<Patient> createPatientDetails(@Valid @RequestBody Patient patient) 
	{
		return new ResponseEntity<Patient>(patientService.createPatientDetails(patient), HttpStatus.CREATED);
		
	}

	@PutMapping(value = "/")
	public ResponseEntity<Patient> updatePatientDetails(@Valid @RequestBody Patient patient) {
		if(!patientRepository.existsById((patient.getPatientId())))
			throw new PatientNotFoundException();
     return new ResponseEntity<Patient>(patientService.updatePatientDetails(patient), HttpStatus.CREATED);
	}

	@DeleteMapping(value = "/{patientId}")

	public String deletePatientDetailsById(@PathVariable Long patientId) {
		
		try{
			if (patientService.DeletePatientdetails(patientId)) {

			return "Deleted Successfully";
		}
	}catch (Exception e) {	}
	
		return "cannot delete details";
	}

}
