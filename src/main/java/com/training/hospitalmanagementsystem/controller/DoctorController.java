package com.training.hospitalmanagementsystem.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.hospitalmanagementsystem.model.Doctor;
import com.training.hospitalmanagementsystem.repository.DoctorRepository;
import com.training.hospitalmanagementsystem.service.DoctorService;
import com.training.hospitalmanagementsystem.config.DoctorNotFoundException;

@RestController
@RequestMapping("/Doctor")
public class DoctorController {
	@Autowired
	private DoctorRepository doctorRepository;
	@Autowired
	DoctorService doctorService;

	@GetMapping("/all")
	public ResponseEntity<List<Doctor>> getAllDoctorDetails() {
		return new ResponseEntity<List<Doctor>>(doctorService.getAllDoctorDetails(), HttpStatus.OK);

	}

	@GetMapping(value = "/{doctorId}")

	public Doctor getOneDoctor(@PathVariable Long doctorId) {

		if (!doctorRepository.existsById(doctorId))
			throw new DoctorNotFoundException();
		return doctorService.getOneDoctor(doctorId);
	}

	@PostMapping(value = "/")
	public ResponseEntity<Doctor> createDoctorDetails(@Valid @RequestBody Doctor doctor) {
		return new ResponseEntity<Doctor>(doctorService.createDoctorDetails(doctor), HttpStatus.CREATED);

	}

	@PutMapping(value = "/")
	public ResponseEntity<Doctor> updateDoctorDetails(@Valid @RequestBody Doctor doctor) {
		if(!doctorRepository.existsById((doctor.getDoctorId())))
			throw new DoctorNotFoundException();
		return new ResponseEntity<Doctor>(doctorService.updateDoctorDetails(doctor), HttpStatus.CREATED);
	}

	@DeleteMapping(value = "/{doctorId}")
	public String deleteDoctorDetailsById(@PathVariable Long doctorId) {
		try {
			if (doctorService.deleteDoctor(doctorId)) {
			return "Deleted Successfully";
			}
			} catch (Exception e) {	
			}
		return "Not able to delete the Doctor";
	
}}
