package com.training.hospitalmanagementsystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.hospitalmanagementsystem.model.Hospital;
//import com.training.hospitalmanagementsystem.model.HospitalDetails;


	public interface HospitalRepository extends JpaRepository<Hospital, Long> {

		
		
	}


