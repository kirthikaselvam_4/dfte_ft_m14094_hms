package com.training.hospitalmanagementsystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.hospitalmanagementsystem.model.Patient;

public interface PatientRepository extends JpaRepository<Patient, Long> {

}