package com.training.hospitalmanagementsystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.hospitalmanagementsystem.model.Appointment;

public interface AppointmentRepository extends JpaRepository<Appointment, Long> {

}