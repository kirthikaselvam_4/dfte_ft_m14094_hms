package com.training.hospitalmanagementsystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.hospitalmanagementsystem.model.Doctor;

public interface DoctorRepository extends JpaRepository<Doctor, Long> {

}
