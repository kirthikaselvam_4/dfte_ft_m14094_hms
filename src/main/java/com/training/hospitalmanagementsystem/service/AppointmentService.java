package com.training.hospitalmanagementsystem.service;

import java.util.List;

import com.training.hospitalmanagementsystem.model.Appointment;

public interface AppointmentService {
	public List<Appointment> getAllAppointmentDetails();
	public Appointment getOneAppointment(Long appointmentId);

	public Appointment createAppointmentDetails(Appointment appointment);

	public Appointment updateAppointmentDetails(Appointment appointment);

	public boolean deleteAppointment(Long appointmentId);

	
}
