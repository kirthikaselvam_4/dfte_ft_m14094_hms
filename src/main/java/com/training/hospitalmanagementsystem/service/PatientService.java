package com.training.hospitalmanagementsystem.service;

import java.util.List;

import com.training.hospitalmanagementsystem.model.Patient;

public interface PatientService {
	public List<Patient> getAllPatientDetails();

	public Patient getOnePatient(Long patientId);

	public Patient createPatientDetails(Patient patient);

	public boolean DeletePatientdetails(Long patientId);

	public Patient updatePatientDetails(Patient patient);


}
