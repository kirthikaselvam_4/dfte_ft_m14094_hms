package com.training.hospitalmanagementsystem.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.training.hospitalmanagementsystem.model.Doctor;
import com.training.hospitalmanagementsystem.repository.DoctorRepository;
@Service
public class DoctorServiceimpl implements DoctorService {
	
	@Autowired
	private DoctorRepository doctorRepository;
	@Override
	public Doctor getOneDoctor(Long doctorId) {
		// TODO Auto-generated method stub
		return doctorRepository.findById(doctorId).orElse(new Doctor());
	}

	@Override
	public List<Doctor> getAllDoctorDetails() {
		// TODO Auto-generated method stub
		return doctorRepository.findAll();
	}

	@Override
	public Doctor createDoctorDetails(@RequestBody Doctor doctor) {
		// TODO Auto-generated method stub
		return doctorRepository.save(doctor);
	}

	@Override
	public Doctor updateDoctorDetails(Doctor doctor) {
		// TODO Auto-generated method stub
		return doctorRepository.save(doctor);
	}

	@Override
	public boolean deleteDoctor(Long doctorId) {
		// TODO Auto-generated method stub
		doctorRepository.deleteById(doctorId);
		return true;
	}

}
