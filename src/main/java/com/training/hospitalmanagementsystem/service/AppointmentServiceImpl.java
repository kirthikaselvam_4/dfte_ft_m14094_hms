package com.training.hospitalmanagementsystem.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.hospitalmanagementsystem.model.Appointment;
import com.training.hospitalmanagementsystem.repository.AppointmentRepository;
@Service
public class AppointmentServiceImpl implements AppointmentService {
	@Autowired
	private AppointmentRepository appointmentRepository;
	
	
	@Override
	public List<Appointment> getAllAppointmentDetails() {
		// TODO Auto-generated method stub
		return appointmentRepository.findAll();
	}
	@Override
	public Appointment getOneAppointment(Long appointmentId) {
	// TODO Auto-generated method stub
	return appointmentRepository.findById(appointmentId).orElse(new Appointment());
	} 
	
	@Override
	public Appointment createAppointmentDetails(Appointment appointment) {
	// TODO Auto-generated method stub
	
	return appointmentRepository.save(appointment);
	} @Override
	public Appointment updateAppointmentDetails(Appointment appointment) {
	// TODO Auto-generated method stub
	
	return appointmentRepository.save(appointment);
	} @Override
	public boolean deleteAppointment(Long appointmentId) {
	// TODO Auto-generated method stub
	appointmentRepository.deleteById(appointmentId);
	return true;
	}

}
