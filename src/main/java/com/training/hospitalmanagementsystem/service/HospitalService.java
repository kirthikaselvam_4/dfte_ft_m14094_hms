package com.training.hospitalmanagementsystem.service;

import java.util.List;

import com.training.hospitalmanagementsystem.model.Hospital;



public interface HospitalService {
	public List<Hospital> getAllHospitalDetails();

	public Hospital getHospitalById(Long hospitalId);

	public Hospital createHospital(Hospital hospital);

	public Hospital updateHospital(Hospital hospital);

	//public boolean deleteDetailsById(Long id);

	public boolean DeleteHospitalById(Long hospitalId);
	
	

	

	

	
	

	

	


	
}
