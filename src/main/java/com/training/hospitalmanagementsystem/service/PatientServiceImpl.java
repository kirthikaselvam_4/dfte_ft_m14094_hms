package com.training.hospitalmanagementsystem.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.hospitalmanagementsystem.model.Patient;
import com.training.hospitalmanagementsystem.repository.PatientRepository;
@Service
public class PatientServiceImpl implements PatientService {
	
@Autowired
	
	private PatientRepository patientRepository;
	@Override
	public List<Patient> getAllPatientDetails() {
	// TODO Auto-generated method stub
	return patientRepository.findAll();
	}



	@Override
	public Patient getOnePatient(Long patientId) {
	// TODO Auto-generated method stub
	return patientRepository.findById(patientId).orElse(new Patient());
	}



	@Override
	public Patient createPatientDetails(Patient patient) {
	// TODO Auto-generated method stub
	
	return patientRepository.save(patient);

	}



	@Override
	public Patient updatePatientDetails(Patient patient) {
	// TODO Auto-generated method stub
	
	return patientRepository.save(patient);

	}



	@Override
	public boolean DeletePatientdetails(Long patientId) {
	// TODO Auto-generated method stub
	patientRepository.deleteById(patientId);
	return true;
	}

}
