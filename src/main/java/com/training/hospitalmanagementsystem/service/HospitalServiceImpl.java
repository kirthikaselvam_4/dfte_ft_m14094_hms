package com.training.hospitalmanagementsystem.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.training.hospitalmanagementsystem.model.Hospital;
import com.training.hospitalmanagementsystem.repository.HospitalRepository;


@Service
public class HospitalServiceImpl implements HospitalService{
	@Autowired
	
	private HospitalRepository repository;
	
	
	@Override
	public List<Hospital> getAllHospitalDetails() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	public Hospital getHospitalById(Long hospitalId) {
		// TODO Auto-generated method stub
		return repository.findById(hospitalId).orElse(new Hospital());
		
	}

	@Override
	public Hospital createHospital(@RequestBody Hospital hospital) {
		// TODO Auto-generated method stub
		return repository.save(hospital);
	}

	@Override
	public Hospital updateHospital(@RequestBody Hospital hospital) {
		// TODO Auto-generated method stub
		return repository.save(hospital);
		
	}
	
	@Override
	public boolean DeleteHospitalById(Long hospitalId) {
		// TODO Auto-generated method stub
		repository.deleteById(hospitalId);
		return true;
	}

	
	
	
/****************************************************************************************/

	
	

	


	
	
	


}
