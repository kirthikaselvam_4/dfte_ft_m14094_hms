package com.training.hospitalmanagementsystem.service;

import java.util.List;

import com.training.hospitalmanagementsystem.model.Doctor;

public interface DoctorService {
	
	public List<Doctor> getAllDoctorDetails();
	public Doctor getOneDoctor(Long doctorId);

	public Doctor createDoctorDetails(Doctor doctor);

	public Doctor updateDoctorDetails(Doctor doctor);

	public boolean deleteDoctor(Long doctorId);

}
