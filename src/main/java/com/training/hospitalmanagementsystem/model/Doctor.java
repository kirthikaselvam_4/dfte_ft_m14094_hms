package com.training.hospitalmanagementsystem.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Doctor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	private Long doctorId;
	@NotEmpty(message="Name cannot be empty")
	@Size(min=3,message="Name should be atleast with 3 characters")
	@Pattern(regexp="^[A-Za-z]*$",message="Enter only charcters")
	private String firstName;
	@Pattern(regexp="^[A-Za-z]*$",message="Enter only charcters")
	private String lastName;
	@NotEmpty
	(message="Department cannot be empty")
	private String departmentName;//specialist
	@ManyToOne(targetEntity = Hospital.class,cascade=CascadeType.ALL)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "refHospitalId", referencedColumnName = "hospitalId")
	private Hospital refHospitalId;
}

//private Long refHospitalId;
	/* working@OneToMany(targetEntity=Appointment.class,cascade=CascadeType.ALL)
  @JoinColumn(name = "refdoctorId", referencedColumnName = "doctorId")
	private List<Appointment> appointment;*/
