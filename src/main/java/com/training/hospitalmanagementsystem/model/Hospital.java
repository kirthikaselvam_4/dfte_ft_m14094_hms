package com.training.hospitalmanagementsystem.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Hospital {
	
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		//private Long serialNo;
		private Long hospitalId;
		@NotEmpty(message="city cannot be null")
		@Pattern(regexp="^[A-Za-z]*$",message="Enter only charcters")
		private String city;
		@NotEmpty(message="Name cannot be null")
		@Pattern(regexp="^[A-Za-z]*$",message="Enter only charcters")
		private String name;
		
}
/*@OneToMany(targetEntity = Doctor.class)
@JoinColumn(name = "refdoctorId", referencedColumnName = "doctorId")
private Doctor refdoctorId;*/

//@OneToMany(targetEntity=Doctor.class,cascade=CascadeType.ALL,orphanRemoval = true)


/*working/*@OneToMany(targetEntity=Doctor.class,cascade=CascadeType.ALL)
@JoinColumn(name="refHospitalId",referencedColumnName="hospitalId")
private List<Doctor> doctor;*/


/*@ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "tutorial_id", nullable = false)
  @OnDelete(action = OnDeleteAction.CASCADE)*/

/*	@ManyToMany(targetEntity=Doctor.class)
@JoinColumn(name = "refDoctorId", referencedColumnName = "doctorId")
private Long refDoctorId;*/
