package com.training.hospitalmanagementsystem.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
//import javax.validation.constraints.NotEmpty;
//import org.hibernate.validator.constraints.NotEmpty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Patient {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long patientId;
	@NotEmpty(message="firstName cannot be empty")
	@Pattern(regexp="^[A-Za-z]*$",message="Enter only charcters")
	@Length(min=3, max= 20,message="Should be minimum of 3 and maximum of 20 character")
	private String firstName;
	@Pattern(regexp="^[A-Za-z]*$",message="Enter only charcters")
	@NotEmpty(message="lastName cannot be empty")
	//@Length(min=3, max= 20,message="Should be minimum of 3 and maximum of 20 character")
	private String lastName;
	private char gender;
	@NotEmpty(message="MbileNo cannot be empty")
	//@Length(min=10,max=10, message="Should contain maximum of 10 numbers")
	 @Pattern(regexp="(^$|[0-9]{10})",message="Should contain maximum of 10 numbers, characters are not allowed")
	//@Range(min = 10,max= 10, message = "phone_no should be exact 10 characters." )
	private String mobileNo;
}