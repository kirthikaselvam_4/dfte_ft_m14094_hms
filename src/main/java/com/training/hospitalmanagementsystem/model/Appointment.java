package com.training.hospitalmanagementsystem.model;

import java.sql.Date;
import java.time.format.DateTimeFormatter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Appointment {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long appointmentId;
    
    
    /*@DateTimeFormat(pattern ="yyyy-MM-dd")
    @NotNull(message="Date cannot be empty")
    private Date appointmentDate;*/
    
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @NotNull(message="Time cannot be empty")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date appointmentDateTime;
    @NotEmpty(message="Status cannot be empty")
    private String Status;
   
    @ManyToOne(targetEntity=Doctor.class)
    @JoinColumn(name = "refDoctorId", referencedColumnName = "doctorId")
    private Doctor refDoctorId;
	//private List<Doctor>  Doctor;
    @ManyToOne(targetEntity=Patient.class)
	@JoinColumn(name="refPatientId",referencedColumnName="patientId")
	private Patient refPatientId ;
    
}
/*
@OneToMany(targetEntity=Doctor.class,cascade=CascadeType.ALL,orphanRemoval = true)
@JoinColumn(name="refHospitalId",referencedColumnName="hospitalId")
private List<Doctor> doctor;*/

//private Long refPatientId;
//@OneToOne(targetEntity=Patient.class,cascade=CascadeType.ALL)
//@JoinColumn(name = "refPatientid",referencedColumnName="patientId",nullable = false, unique = true)
//@OneToOne
// @JoinColumn(name = "refPatientid",referencedColumnName="patientId")
//private Long refPatientId;
/* @ManyToOne(targetEntity=Patient.class,cascade=CascadeType.ALL)
@JoinColumn(name="refPatientId",referencedColumnName="patientId")
private List<Patient>  Patient;*/