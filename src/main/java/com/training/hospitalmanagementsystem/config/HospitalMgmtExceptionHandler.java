package com.training.hospitalmanagementsystem.config;

import java.util.HashMap;
import java.util.Map;

import org.hibernate.exception.JDBCConnectionException;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class HospitalMgmtExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String message = error.getDefaultMessage();
			errors.put(fieldName, message);

		});
		return new ResponseEntity<Object>(errors, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = DoctorNotFoundException.class)
	public ResponseEntity<String> exception(DoctorNotFoundException exception) {
		return new ResponseEntity<>("Doctor not found in the databsae, please try again with the different DoctorId",
				HttpStatus.NOT_FOUND);
	}
	@ExceptionHandler(value = HospitalNotFoundException.class)
	public ResponseEntity<String> exception(HospitalNotFoundException exception) {
		return new ResponseEntity<>("Hospital not found in the databsae for the given Id, please try again with the different HospitalId",
				HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(value = { JDBCConnectionException.class })
	public ResponseEntity<String> resourceNotFoundException(ResourceNotFoundException ex, WebRequest request) {

		return new ResponseEntity<String>("Database is down, please try after some time", HttpStatus.NOT_FOUND);
	}
	@ExceptionHandler(value = PatientNotFoundException.class)
	public ResponseEntity<String> exception(PatientNotFoundException exception) {
		return new ResponseEntity<>("Patient not found in the databsae, please try again with the different PatientId",
				HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(value = AppointmentNotFoundException.class)
	public ResponseEntity<String> exception(AppointmentNotFoundException exception) {
		return new ResponseEntity<>("Appointment not found in the databsae, please try again with the different AppointmentId",
				HttpStatus.NOT_FOUND);
	}
	
	
	
	
	


}